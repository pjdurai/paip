
(defun mappend(fn list)
  (if (null list)
      nil
      (append (funcall fn (car list))
              (mappend fn (cdr list)))))

(defun random-elt (choices)
  (elt choices (random (length choices))))

(defun one-of (choices)
  (list (random-elt choices)))

(defparameter *simple-grammer*
  '((sentence -> (noun-phrase verb-phrase))
    (noun-phrase -> (Article Noun))
    (verb-phrase -> (Verb noun-phrase))
    (Article -> a the)
    (Noun -> man ball woman table)
    (Verb -> hit took saw liked)))


(defconstant *grammer*  *simple-grammer*)


(defun rule-lhs(rule)
  (car rule))

(defun rule-rhs(rule)
  (rest (rest rule)))

(defun rewrites(category)
  (rule-rhs (assoc category *grammer*)))


(defun generate(phrase)
  (cond ((listp phrase)
         (mappend #'generate phrase))
         ((rewrites phrase)
          (generate (random-elt (rewrites phrase))))
         (t (list phrase))))


